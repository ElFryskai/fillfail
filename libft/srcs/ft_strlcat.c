/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:44:07 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 11:44:38 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dest, const char *src, size_t n)
{
	unsigned int	i;
	unsigned int	j;
	int				destbefore;

	i = -1;
	j = -1;
	destbefore = ft_strlen(dest);
	if (n > 0)
	{
		while (dest[++i])
			;
		while (src[++j] && i < n - 1)
			dest[i++] = src[j];
		dest[i] = '\0';
	}
	if (n < ft_strlen(dest))
		return (ft_strlen(src) + n);
	return (destbefore + ft_strlen(src));
}
