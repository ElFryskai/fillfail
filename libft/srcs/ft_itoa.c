/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:13:43 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 20:04:01 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	char	*result;
	char	*digit;
	long	nbr;
	int		len;
	int		i;

	nbr = n;
	digit = ft_strdup("0123456789");
	len = ft_longlen(n);
	if (!(result = (char *)malloc(sizeof(*result) * (len + 1))))
		return (NULL);
	ft_bzero(result, len + 1);
	if (nbr < 0)
		nbr = -nbr;
	i = len - 1;
	while (i >= 0)
	{
		result[i--] = digit[nbr % 10];
		nbr /= 10;
	}
	if (n < 0)
		result[0] = '-';
	return (result);
}
