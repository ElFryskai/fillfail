/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sortstrtab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 22:13:56 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 22:16:06 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_sortstrtab(char **tab, size_t size)
{
	unsigned int	i;

	i = 0;
	while (++i < size)
	{
		if (ft_strcmp(tab[i], tab[i - 1]) < 0)
		{
			ft_strswap(&tab[i], &tab[i - 1]);
			i = 0;
		}
	}
}
