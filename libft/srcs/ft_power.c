/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_power.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:30:12 by groussel          #+#    #+#             */
/*   Updated: 2018/04/05 22:17:49 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_power(int n, int pow)
{
	if (pow < 0)
		return (0);
	if (pow == 0)
		return (1);
	return (n * ft_power(n, pow - 1));
}
