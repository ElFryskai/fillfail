/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 10:18:59 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 14:45:17 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *nptr)
{
	unsigned long	result;
	int				neg;
	int				i;

	i = -1;
	neg = 0;
	result = 0;
	while (ft_isspace(nptr[++i]))
		;
	if (nptr[i] == '-' || nptr[i] == '+')
	{
		if (nptr[i] == '-')
			neg = 1;
		i++;
	}
	if (nptr[i] == '-' || nptr[i] == '+' || !ft_isdigit(nptr[i]))
		return (0);
	i--;
	while (ft_isdigit(nptr[++i]))
		result = result * 10 + nptr[i] - '0';
	if (neg)
		return (-result);
	return (result);
}
