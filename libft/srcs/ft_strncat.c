/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:47:21 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 11:47:24 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	unsigned int	i;
	unsigned int	j;

	i = -1;
	j = -1;
	while (dest[++i])
		;
	while (src[++j] && j < n)
		dest[i++] = src[j];
	dest[i] = '\0';
	return (dest);
}
