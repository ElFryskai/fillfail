/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:18:38 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 11:19:02 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	unsigned int	i;
	unsigned char	*strdest;
	unsigned char	*strsrc;

	i = -1;
	strsrc = (unsigned char *)src;
	strdest = (unsigned char *)dest;
	while (++i < n)
		strdest[i] = strsrc[i];
	dest = strdest;
	return (dest);
}
