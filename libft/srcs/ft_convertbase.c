/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convertbase.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:08:27 by groussel          #+#    #+#             */
/*   Updated: 2018/04/05 22:23:23 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		tobaseten(const char *s, const char *base)
{
	int		power;
	int		result;
	int		i;

	power = 0;
	result = 0;
	i = ft_strlen(s);
	while (--i >= 0)
	{
		result += ft_lentill(base, s[i]) * ft_power(ft_strlen(base), power);
		power++;
	}
	return (result);
}

static char		*tobasex(long decimal, const char *base)
{
	char	*tmp;
	int		i;

	if (!(tmp = (char *)ft_memalloc(ft_longlen(decimal) + 1)))
		return (NULL);
	i = -1;
	while (decimal)
	{
		tmp[++i] = base[decimal % ft_strlen(base)];
		decimal /= ft_strlen(base);
	}
	return (tmp);
}

static int		checkdouble(const char *str)
{
	unsigned int	i;
	unsigned int	j;

	i = -1;
	while (++i < ft_strlen(str))
	{
		j = i;
		while (++j < ft_strlen(str))
		{
			if (str[i] == str[j])
				return (0);
		}
	}
	return (1);
}

static int		isinclude(const char *str, const char *base)
{
	unsigned int	i;

	i = -1;
	while (++i < ft_strlen(base))
	{
		if (!ft_strchr(base, str[i]))
			return (0);
	}
	return (1);
}

char			*ft_convertbase(const char *n, const char *from, const char *to)
{
	int		baseten;
	char	*result;

	if (!checkdouble(from) || !checkdouble(to) || !isinclude(n, from))
		return (NULL);
	baseten = tobaseten(n, from);
	result = tobasex(baseten, to);
	result = ft_reversetab(result);
	return (result);
}
