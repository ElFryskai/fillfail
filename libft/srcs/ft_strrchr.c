/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:50:21 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 17:16:37 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	unsigned int	i;
	int				tmp;

	i = -1;
	tmp = -1;
	while (s[++i])
	{
		if (s[i] == c)
			tmp = i;
	}
	if (s[i] == c)
		tmp = i;
	if (tmp >= 0)
		return ((char *)&s[tmp]);
	return (NULL);
}
