/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:52:19 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 11:52:22 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *haystack, const char *needle)
{
	unsigned int	i;
	unsigned int	j;

	i = -1;
	if (ft_strcmp(haystack, needle) == 0)
		return ((char *)haystack);
	while (haystack[++i])
	{
		j = -1;
		while (haystack[i] == needle[++j] && needle[j])
			i++;
		if (needle[j] == '\0')
			return ((char *)&haystack[i - j]);
		else
			i -= j;
	}
	return (NULL);
}
