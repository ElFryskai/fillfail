/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:17:53 by groussel          #+#    #+#             */
/*   Updated: 2018/04/05 12:09:26 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned int	i;
	unsigned char	*tmp;

	tmp = (unsigned char *)s;
	i = -1;
	while (++i < n)
	{
		if (tmp[i] == (unsigned char)c)
			return ((void *)&tmp[i]);
	}
	return (NULL);
}
