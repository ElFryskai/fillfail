/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_reversetab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:40:03 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 20:06:03 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_reversetab(char *tab)
{
	char			*tmp;
	unsigned int	i;
	unsigned int	j;

	if (!(tmp = (char *)ft_memalloc(ft_strlen(tab))))
		return (NULL);
	i = -1;
	j = ft_strlen(tab);
	while (++i < ft_strlen(tab))
		tmp[i] = tab[--j];
	return (tmp);
}
