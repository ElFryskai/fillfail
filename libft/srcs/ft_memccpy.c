/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 11:16:30 by groussel          #+#    #+#             */
/*   Updated: 2018/04/05 14:45:19 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned int	i;
	unsigned char	*strdest;
	unsigned char	*strsrc;

	i = -1;
	strdest = (unsigned char *)dest;
	strsrc = (unsigned char *)src;
	while (++i < n)
	{
		strdest[i] = strsrc[i];
		if (strsrc[i] == (unsigned char)c)
		{
			dest = strdest;
			return (&dest[i + 1]);
		}
	}
	return (NULL);
}
