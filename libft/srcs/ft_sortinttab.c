/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sortinttab.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: groussel <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/04 22:13:32 by groussel          #+#    #+#             */
/*   Updated: 2018/04/04 22:13:34 by groussel         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_sortinttab(int *tab, size_t size)
{
	unsigned int	i;

	i = 0;
	while (++i < size)
	{
		if (tab[i] < tab[i - 1])
		{
			ft_intswap(&tab[i], &tab[i - 1]);
			i = 0;
		}
	}
}
